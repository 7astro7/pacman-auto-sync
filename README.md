
# pacman-auto-sync
## Automate Arch Linux package updates 

pacman-auto-sync uses a systemd service and timer to serve upgrades of out-of-date system packages and integrate changes to repository databases. Every 3 hours the timer pacman-auto-sync.timer activates pacman-auto-sync.service. pacman-auto-sync.service instructs pacman to upgrade packages with minimal verbosity. 

Please read the [Upgrading the System section on Arch Wiki's System Maintenance page](https://wiki.archlinux.org/index.php/System_maintenance#Upgrading_the_systembefore) before installing pacman-auto-sync. It's strongly recommended that pacman-auto-sync be used in tandem with the [informant AUR package](https://github.com/bradford-smith94/informant) that ensures you've read latest Arch News before updating. 

## Package Status

Not ready

## Why a service and timer instead of cron? 
- Easier debugging may distinguish systemd timer / service strategies from those relying on cron. Systemd has standard log data in ~/var/log/journal. For more on systemd and cron see [Arch Wiki's systemd/Timer As a cron replacement](https://wiki.archlinux.org/index.php/Systemd/Timers)

## Installation 

__Clone the repo and cd into it__

    $ git clone git@github.com:7astro7/pacman-auto-sync

    
    $ cd pacman-auto-sync

__Build the package__

    $ makepkg 

To test timer schedules other than every 3 hours see __systemd-analyze calendar__ section of [systemd-analyze manpage](https://man.archlinux.org/man/core/systemd/systemd-analyze.1.en)

## Issues 
All issues, patches, questions, recommendations are welcomed and appreciated. Open an issue or send to zaknyy@protonmail.com. 

## Credit

Source code original found here: https://www.techrapid.uk/2017/04/automatically-update-arch-linux-with-systemd.html

Forked from https://github.com/cmuench/pacman-auto-update
